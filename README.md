# Java

Este repositorio contiene ejercicios de practica en __Java__. Mi intención es crear una colección de ejemplos que se enfoquen en una mecánica del lenguaje de programación Java y sirvan de referencia para poder usar estas mecánicas en proyectos en el futuro.

## ¿Que usé para crear estos programas?

Utilizo una computadora de escritorio con el sistema operativo Ubuntu Desktop 18.04 y el IDE [Intellij Idea Community Edition], también he instalado [Java versión 10.0.1] junto con el SDK de la misma versión.

## Folders o Directorios.

He creado una estructura simple, en el directorio raiz __/__ existe este archivo `README.md` y junto a el existen los folders:

- __Diario__: Contiene archivos de texto con una descripcion del progreso del día. Aqui encontraras una coleccion de mis ideas y mi proceso de pensamiento.

- __Ejercicios__: Contiene subdirectorios los cuales contienen los proyectos de __Intellij Idea__, los cuales a su vez contienen el código fuente de los mismos. Los subdirectorios son enumerados para facilitar su ordenamiento cronológico (ejemplo. `001_HolaMundo/` y `002_HolaMundoArgs/`)

[Intellij Idea Community Edition]: https://www.jetbrains.com/idea/download/
[Java versión 10.0.1]: http://www.oracle.com/technetwork/java/javase/downloads/jdk10-downloads-4416644.html

