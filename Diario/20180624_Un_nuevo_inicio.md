# 2018-06-24  Un nuevo inicio.

## 15:09

A poco del anuncio de que [Microsoft compra GitHub por 7,500 millones de dólares] y teniendo en cuenta el historial de Microsoft de hundir todo lo que compra, por ejemplo Skype. Me veo forzado a abandonar el barco mientras aun flote.

Cree una cuenta gratuita en [Gitlab] y es ahí donde he creado este repositorio.

Me alegra ver que no es dificil de utilizar y teniendo los conocimientos de Git, no es dificil entender como puedo crear mis proyectos y seguir usandolos en la nube.

Ya tengo hambre, me dare un baño y buscare algo de comer.

[Microsoft compra GitHub por 7,500 millones de dólares]: https://www.xataka.com/aplicaciones/oficial-microsoft-compra-github-7-500-millones-dolares
[Gitlab]: https://gitlab.com/
