# 2018-06-24 Probando clases.

## 13:28

Estoy algo avanzado con el curso de [Java Fundamentals: The Java Language] de hecho llevo un año repasando ese curso y muchos otros, y no me he dado a la tarea de practicar fuera de los ejemplos que vienen en el curso.

Hoy empiezo con la creacion de proyectos cuyo fín es:

- Plantear la mecanica a poner en práctica.
- Exponer el problema que dicha mecanica resuelve.
- Implementar la solución.
- Exponer conclusiones.

Los anteriores puntos los expongo en forma de comentarios dentro del mismo código fuente.

El primer ejercicio será `/Ejercicios/001_HolaMundoArgs/` el cual contiene un programa sencillo que usa los argumentos de entrada.

El segundo ejercicio será mucho más complicado, `/Ejercicios/002_Clases/` contiene el ejercicio de creacion de una clase muy sencilla con el uso de constructores, propiedades y métodos. Como con las clases se pueden hacer muchas otras cosas, estas se quedaran fuera del alcance de este ejercicio.

## 14:50

Acabo de realizar los dos ultimos ejemplos.

Ahora quiero empezar a practicar las mecanicas con clases como:

- Inizializadores de clases.
- Constructores.

[Java Fundamentals: The Java Language]: https://app.pluralsight.com/library/courses/java-fundamentals-language/table-of-contents