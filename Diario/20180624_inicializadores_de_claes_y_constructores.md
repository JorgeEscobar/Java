# 2018-06-24  Inicializadores de clases y constructores.

## 20:07

Este proyecto es un repaso de las mecanicas de inicializacion o lo que es lo mismo, crear objetos con valores _válidos_ sin nececidad de exigir esta eresponsabilidad al programador.

Los temas que vere son:

- Estableciendo un estado incial.
- Inicializadores de campos.
- Constructores.
- Encadenar constructores y visibilidad.
- Bloques de inicializacion.
- Orden de inicializacion y construccion.

