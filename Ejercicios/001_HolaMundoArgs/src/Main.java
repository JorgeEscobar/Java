/*MIT License

Copyright (c) 2018 Jorge Ricardo Escobar Carrasco.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.*/

/*
* Este programa hace uso de los argumentos que se pasan en tiempo de ejecucion para imprimir un mensaje.
*
* Para pasar argumentos, se debe ejecutar el programa desde la línea de comandos, por ejemplo:
*
* $ java -classpath C:\Users\Ricardo\Documents\Gitlab\Java\Ejercicios\001_HolaMundoArgs\out\production\001_HolaMundoArgs Main
¡Hola Mundo!
*
* por algun motivo necesito agregar el parametro -classpath para poder ejecutar el programa.
* */
public class Main {

    public static void main(String[] args) {

        // Si el numero de argumentos es 0, se imprime otro mensaje.

        if (args.length > 0) {
            System.out.println("Hola " + args[0]); // Se imprime el mensaje con el primer argumento
        } else {
            System.out.println("¡Hola Mundo!"); // Se imprime "¡Hola Mundo!" cuando no existe el argumento
        }
    }
}
