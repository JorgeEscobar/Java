/*MIT License

Copyright (c) 2018 Jorge Ricardo Escobar Carrasco

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.*/

package com.ricardo;

import com.ricardo.dinero.Dolar;
import com.ricardo.dinero.Euro;

import java.math.BigDecimal;

public class Main {

    public static void main(String[] args) {
        System.out.println("Interfaces Personalizadas");

        Dolar dolar = new Dolar("20.15");
        Euro euro = new Euro("13.8");

        float dineroFloat = 0.0F;
        double dineroDouble = 0.0D;
        BigDecimal dineroBigDecimal = new BigDecimal("0.0");
        BigDecimal unCentavo = new BigDecimal("0.01");
        BigDecimal resultado = null;

        for (int i = 0; i < 100; i++) {
            dineroDouble += 0.01D;
            dineroFloat += 0.01F;
            resultado = new BigDecimal(dineroBigDecimal.toString()).add(unCentavo);
            dineroBigDecimal = new BigDecimal(resultado.toString());
        }

        System.out.println("DineroDouble = "+ dineroDouble);
        System.out.println("DineroFloat = "+ dineroFloat);
        System.out.println("dineroBigDecimal = "+ resultado);

        System.out.println(dolar);
        System.out.println(euro);
    }
}
