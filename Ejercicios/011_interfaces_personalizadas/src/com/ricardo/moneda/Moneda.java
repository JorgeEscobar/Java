package com.ricardo.moneda;

import java.math.BigDecimal;

public interface Moneda {
    char getSimbolo();
    String SEPARADOR = ",";
    BigDecimal suma(BigDecimal valor);
}
