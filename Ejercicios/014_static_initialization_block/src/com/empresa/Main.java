/*MIT License

Copyright (c) 2018 Jorge Ricardo Escobar Carrasco

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.*/

/* Este programa crea objetos a partir de un archivo de texto. Esto puede ser usado
*  para inicializar con datos externos (archivo de texto) los objetos que se vayan a usar.
*  Mi primera idea seria usar esta teecnica para inicializar los datos de conexion de una
*  base de datos en el programa y poder asi cambiar los mismos datos de conexion a
*  traves de un archivo de texto.*/

package com.empresa;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    // TODO: Investigar como obtener el directorio en el que se encuentra la aplicacion y usarlo como ruta dinamica al archivo


    public final static String ARCHIVO = "C:\\conexion.txt";
    private static String cadenaDeConexion = null;

    // static initialization block
    static {
        BufferedReader reader = null;

        try {
            File archivo = new File(ARCHIVO);
            reader = new BufferedReader(new FileReader(archivo));
            String linea = null;
            while ((linea = reader.readLine()) != null) {
                cadenaDeConexion = linea.toString();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        // Esta cadena almacenara la cadena de conexion a la base de datos
        System.out.println(ARCHIVO);
        System.out.println(cadenaDeConexion);

    }
}
