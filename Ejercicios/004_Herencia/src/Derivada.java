/*MIT License

Copyright (c) 2018 Jorge Ricardo Escobar Carrasco.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.*/

import java.util.Currency;
import java.util.Locale;

public class Derivada extends Base {

    // Campos privados de la clase Derivada

    private boolean hayFondos = false;
    private double dinero = 0.0D;
    private Locale locale;
    private Currency currency;

    // Constructores

    public Derivada() {
        locale = new Locale("es_MX");
        currency = new Currency(locale);
    }

    public Derivada(double dinero) {
        this.dinero= dinero;
        this.hayFondos = (this.dinero > 0.0D) ? true : false;
        locale = new Locale("es_MX");
        currency = new Currency(locale);
    }

    public Derivada(int rango, double dinero) {
        super(rango);
        this.dinero = dinero;
        locale = new Locale("es_MX");
        currency = new Currency(locale);
    }

    // SETTERS & GETTERS

    public double getDinero() {
        return dinero;
    }

    // Metodos de la clase

    public void depositar(double dinero) {
        this.dinero += dinero;
    }

    private boolean hayFondosParaRetirar (double dinero) {
        return (this.dinero >= dinero) ? true : false;
    }

    public void retirar(double dinero) {
       if (hayFondosParaRetirar(dinero)) {
           this.dinero -= dinero;
       } else {
           System.out.println("Fondos insuficientes.");
       }
    }

    public void transferencia(Derivada cuentaDestino, double dinero) {
        if (hayFondosParaRetirar(dinero)) {
            this.dinero -= dinero;
            cuentaDestino.depositar(dinero);
        } else {
            System.out.println("Fondos insuficientes.");
        }
    }
}
