/*MIT License

Copyright (c) 2018 Jorge Ricardo Escobar Carrasco.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.*/

public class Base {

    // Campos privados de la clase

    private int rango = 10;
    private String descripcion = null;

    // Constructores

    public Base() {
        estableceRango(this.rango);
    }

    public Base(int rango) {
        estableceRango(rango);
    }

    // GETTERS & SETTERS

    public int getRango() {
        return rango;
    }

    public void setRango(int rango) {
        this.estableceRango(rango);
    }

    public String getDescripcion() {
        return descripcion;
    }

    // Metodos de la clase

    private void estableceRango(int rango) {
        this.rango = rango;
        this.descripcion = this.obtenerDescripcion();
    }

    private String obtenerDescripcion() {
        // Establece el rango con un switch
        switch (this.rango) {
            case 3:
                return "Cuenta basica";
            case 2:
                return "Cuenta normal";
            case 1:
                return "Cuenta premium";
            case 0:
                return "Cuenta secreta";
            default:
                return "Rango no registrado";
        }
    }
}
