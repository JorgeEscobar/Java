/*  MIT License

    Copyright (c) 2018 Jorge Ricardo Escobar Carrasco

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.*/

package com.empresa;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        System.out.println("2018-09-06 Crea un archivo de texto con el contenido del array String.");

        // Crea el arreglo de cadenas de caracteres (Array String)
        String[] data = {"Linea 1", "Linea 2 2", "Linea 3 3 3", "Linea 4 4 4 4", "Linea 5,5,5,5,5"};

        try {
            // Invoca el metodo para escribir datos
            writeData(data);
            // Invoca el metodo para leer datos
            readData();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    // METODO PARA ESCRIBIR EN UN ARCHIVO

    static void writeData(String[] data) throws IOException {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("data.txt"))) {
            for (String dataLine : data) {
                bufferedWriter.write(dataLine);
                bufferedWriter.newLine();
            }
        }
    }

    static void readData() throws IOException {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("data.txt"))) {
            String inValue;
            while ((inValue = bufferedReader.readLine()) !=  null) {
                System.out.println(inValue);
            }
        }
    }
}
