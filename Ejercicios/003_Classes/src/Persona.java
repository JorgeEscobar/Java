/*MIT License

Copyright (c) 2018 Jorge Ricardo Escobar Carrasco.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.*/

/*
* Esta clase usa mecanicas basicas para almacenar el nombre completo de una persona.
* */

public class Persona {

    // Campos privados, no pueden ser alterados fuera de la clase. Se requieren métodos públicos para acceder a ellos.

    private String nombres;
    private String apellidoPaterno;
    private String apellidoMaterno;

    /*  Getters y Setters. Estos métodos se usan para leer y escribir los campos privados desde fuera de la clase.

        En sí los Getters son métodos que retornan el valor o referencia de un campo provado especifico. Existe un
        método Getter por cada campo privado en este ejercicio.

        Los Setters son métodos públicos que guardan un nuevo valor o referencia en los campos privados, igual que los
        setters, existe uno por cada campo en este ejercicio.
    */

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    // Metodo publico para imprimir los valores de los campos privados

    public String getNombreCompleto() {
        return this.nombres + " " + this.getApellidoPaterno() + " " + this.getApellidoMaterno();
    }
}
