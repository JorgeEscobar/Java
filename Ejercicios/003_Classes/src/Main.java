/*MIT License

Copyright (c) 2018 Jorge Ricardo Escobar Carrasco.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.*/

/*
* Este proyecto tiene como objetivo practicar varias mecanicas básicas sobre las clases en Java.
*
* REPRESENTAR TIPOS COMPLEJOS CON CLASES:
*
* 1. Creando y usando clases.
* 2. Encapsulacion y modificadores de acceso.
* 3. Aplicando modificadores de acceso.
* 4. Lo básico sobre los Metodos.
* 5. Saliendo de un método.
* 6. valores de retorno de un método.
* 7. Referencias especiales: THIS y NULL.
* 8. Encapsulacion de campos, GETTERS y SETTERS.
*
* */

public class Main {

    public static void main(String[] args) {
        // Programa principal, checar el archivo Persona.java para ver como se crea una clase.

        Persona jorge; // Crea una REFERENCIA llamada "jorge" del tipo "Persona"
        jorge = new Persona(); // Crea una INSTANCIA del tipo "Persona" y la asigna a la REFERENCIA llamada "jorge"

        // Asigna valores a la instancia "jorge"
        jorge.setNombres("Jorge Ricardo");
        jorge.setApellidoPaterno("Escobar");
        jorge.setApellidoMaterno("Carrasco");

        // Imprime el nombre completo
        System.out.println("Clase: "+ jorge.getClass().getName());
        System.out.println("Nombre completo: "+ jorge.getNombreCompleto());

        /* Se comenta este bloque ya que fue reemplazado por una invocacion al método "getNombreCompleto"

        // Imprime el nombre de la clase y los valores de los campos privados.
        System.out.println("Clase: "+ jorge.getClass().getName());
        System.out.println("Nombre: "+ jorge.getNombres());
        System.out.println("Apellido paterno: "+ jorge.getApellidoPaterno());
        System.out.println("Apellido materno: "+ jorge.getApellidoMaterno());*/

    }
}
