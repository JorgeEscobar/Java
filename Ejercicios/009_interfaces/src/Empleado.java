public class Empleado implements Comparable {

    // Campos miembro

    private String nombre;
    private int rango; // 1 = bronce, 2 = plata, 3 = oro (1º prioridad)
    private int antiguedad; // expresado en dias laborados

    // Constructores

    public Empleado() {}

    public Empleado(String nombre, int rango, int antiguedad) {
        this.nombre = nombre;
        this.rango = rango;
        this.antiguedad = antiguedad;
    }


    @Override
    public int compareTo(Object o) {
        // Convierte la referencia al objeto a una referencia tipo Empleado
        Empleado empleado = (Empleado) o;

        if (rango > empleado.rango)
            return -1;
        else if (rango < empleado.rango)
            return 1;
        else {
            if (antiguedad > empleado.antiguedad)
                return -1;
            else if (rango < empleado.rango)
                return 1;
            else
                return 0;
        }
    }

    @Override
    public String toString() {
        return nombre + ", " + rango + ", " + antiguedad;
    }
}
