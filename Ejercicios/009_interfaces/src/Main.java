/*MIT License

Copyright (c) 2018 Jorge Ricardo Escobar Carrasco

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        Empleado bob = new Empleado("Bob", 1, 15);
        Empleado jane = new Empleado("Jane", 2, 259);
        Empleado jane2 = new Empleado("Jane2", 2, 259);
        Empleado steve = new Empleado("Steve", 3, 1690);
        Empleado lisa = new Empleado("Lisa", 3, 1501);

        Empleado[] empleados = {bob, jane, jane2, steve, lisa};

        for (Empleado e:empleados) {
            System.out.println(e);

        }

        System.out.println();
        Arrays.sort(empleados);
        System.out.println();

        for (Empleado e:empleados) {
            System.out.println(e);
        }

    }
}
