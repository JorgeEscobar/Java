/*MIT License

Copyright (c) 2018 Jorge Ricardo Escobar Carrasco

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.*/

/**
 * Este programa crea una clase "ExcepcionPersonalizada" la cual captura errores a la consola.
 */

public class Main {

    public static void main(String[] args) {
        System.out.println("ExcepcionPersonalizada 2018.");

        double numero;
        String[] declaraciones = {"2.0D", "-3.5", "X.4", "0.0D", "0.0", "0", "2018.06d"};

        System.out.println();
        System.out.println("Convertir un numero guardado como String a Double: ");

        for (String declaracion : declaraciones) {
            System.out.println(declaracion + ", ");
            try {
                try {
                    numero = Double.valueOf(declaracion);
                } catch (NumberFormatException e) {
                    throw new ExcepcionPersonalizada("El dato no es un numero", declaracion, e);
                }

                if (numero == 0) {
                    throw new ExcepcionPersonalizada("El numero no puede ser 0", declaracion);
                } else if (numero < 0) {
                    throw new ExcepcionPersonalizada("El numero no puede ser negativo", declaracion);
                }
            } catch (ExcepcionPersonalizada e) {
                System.out.println(e.getMessage());
                if (e.getCause() != null)
                    System.out.println("  Excepcion original: " + e.getCause().getMessage());
            }
        }
    }
}

