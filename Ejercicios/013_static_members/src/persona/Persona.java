package persona;

public class Persona {
    String nombre;
    int edad;

    // constructores

    public Persona() {
        totalPersonas++;
    }

    public Persona(String nombre, int edad) {
        this();
        this.nombre = nombre;
        this.edad = edad;
    }

    // static members

    static int totalPersonas;

    public static int getTotalPersonas() {
        return totalPersonas;
    }

}
