import java.util.Iterator;

public class Oficina implements Comparable<Oficina>, Iterable<Persona> {

    // Campos miembro

    private int rango;
    private Jefe[] jefes;
    private Empleado[] empleados;

    // Constructor

    public Oficina() {}

    public Oficina(int rango) {
        this.rango = rango;
    }

    public Oficina(Jefe[] jefes, Empleado[] empleados, int rango) {
        this(rango);
        this.jefes = jefes;
        this.empleados = empleados;
    }

// Implementacion de la interface Comparable e Itereable

    @Override
    public int compareTo(Oficina oficina) {
        return rango - oficina.rango;
    }

    @Override
    public Iterator<Persona> iterator() {
        return new OficinaIterator(jefes, empleados);
    }
}
