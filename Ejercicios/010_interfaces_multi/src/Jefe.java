public class Jefe extends Persona {

    // Campos miembro

    private int numeroEmpleados;

    public Jefe(String nombre, int numeroEmpleados) {
        super(nombre);
        this.numeroEmpleados = numeroEmpleados;
    }
}
