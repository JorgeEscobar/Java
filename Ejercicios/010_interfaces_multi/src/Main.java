/*MIT License

Copyright (c) 2018 Jorge Ricardo Escobar Carrasco

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.*/

public class Main {

    public static void main(String[] args) {
	    System.out.println("Implementing Multiple Interfaces.");
        System.out.println();

        // Agrega miembros de jefes y empleados

        Empleado[] empleados = {
                new Empleado("Lisa", 2),
                new Empleado("Juan", 5)};

        Jefe[] jefes = {
                new Jefe("Jose", 100)
        };

        // Crea una oficina

        Oficina oficina01 = new Oficina(jefes, empleados, 1);

        // Uso de la interface Itereable e Iterator

        for (Persona persona:oficina01) {
            System.out.println(persona);
        }
    }
}
